$(function()
{
    $('.form-date').datetimepicker('setEndDate', today);
    $('#left').parents('tr').toggle($('#objectType').val() == 'task');
    $('#objectType').change(function()
    {
        $('#left').parents('tr').toggle($('#objectType').val() == 'task');
    });
})
