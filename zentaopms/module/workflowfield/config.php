<?php
$config->workflowfield->optionControls = array('select', 'multi-select', 'checkbox', 'radio');
$config->workflowfield->remainFields   = array('subStatus', 'file', 'actions');  // The system remain fields.

$config->workflowfield->require = new stdclass();
$config->workflowfield->require->create = 'name, field, module, control';
$config->workflowfield->require->edit   = 'name, field, control';

$config->workflowfield->disabledFields['subTables'] = 'status,assignedTo,editedBy,editedDate,assignedBy,assignedDate,mailto,deleted';

$config->workflowfield->typeList['integer']['tinyint']   = 'tinyint';
$config->workflowfield->typeList['integer']['smallint']  = 'smallint';
$config->workflowfield->typeList['integer']['mediumint'] = 'mediumint';
$config->workflowfield->typeList['integer']['int']       = 'int';

$config->workflowfield->typeList['decimal']['decimal'] = 'decimal';

$config->workflowfield->typeList['date']['date'] = 'date';

$config->workflowfield->typeList['time']['datetime'] = 'datetime';

$config->workflowfield->typeList['varchar']['char']    = 'char';
$config->workflowfield->typeList['varchar']['varchar'] = 'varchar';

$config->workflowfield->typeList['text']['text'] = 'text';

$config->workflowfield->lengthList[10]  = ',date,';
$config->workflowfield->lengthList[19]  = ',datetime,';
$config->workflowfield->lengthList[200] = ',input,select,radio,checkbox,';

$config->workflowfield->numberTypes = array_merge(array_keys($config->workflowfield->typeList['integer']), array_keys($config->workflowfield->typeList['decimal']));

$config->workflowfield->default = new stdclass();
$config->workflowfield->default->fields['id']           = 'mediumint(8) unsigned NOT NULL AUTO_INCREMENT';
$config->workflowfield->default->fields['parent']       = 'mediumint(8) unsigned NOT NULL';
$config->workflowfield->default->fields['assignedTo']   = 'varchar(30) NOT NULL';
$config->workflowfield->default->fields['status']       = 'varchar(30) NOT NULL';
$config->workflowfield->default->fields['createdBy']    = 'varchar(30) NOT NULL';
$config->workflowfield->default->fields['createdDate']  = 'datetime NOT NULL';
$config->workflowfield->default->fields['editedBy']     = 'varchar(30) NOT NULL';
$config->workflowfield->default->fields['editedDate']   = 'datetime NOT NULL';
$config->workflowfield->default->fields['assignedBy']   = 'varchar(30) NOT NULL';
$config->workflowfield->default->fields['assignedDate'] = 'datetime NOT NULL';
$config->workflowfield->default->fields['mailto']       = 'text NOT NULL';
$config->workflowfield->default->fields['deleted']      = "enum('0', '1') NOT NULL DEFAULT '0'";

$config->workflowfield->default->fieldTypes['id']           = 'mediumint';
$config->workflowfield->default->fieldTypes['parent']       = 'mediumint';
$config->workflowfield->default->fieldTypes['assignedTo']   = 'varchar';
$config->workflowfield->default->fieldTypes['status']       = 'varchar';
$config->workflowfield->default->fieldTypes['createdBy']    = 'varchar';
$config->workflowfield->default->fieldTypes['createdDate']  = 'datetime';
$config->workflowfield->default->fieldTypes['editedBy']     = 'varchar';
$config->workflowfield->default->fieldTypes['editedDate']   = 'datetime';
$config->workflowfield->default->fieldTypes['assignedBy']   = 'varchar';
$config->workflowfield->default->fieldTypes['assignedDate'] = 'datetime';
$config->workflowfield->default->fieldTypes['mailto']       = 'text';
$config->workflowfield->default->fieldTypes['deleted']      = 'varchar';

$config->workflowfield->default->fieldLength['id']           = '8';
$config->workflowfield->default->fieldLength['parent']       = '8';
$config->workflowfield->default->fieldLength['assignedTo']   = '30';
$config->workflowfield->default->fieldLength['status']       = '30';
$config->workflowfield->default->fieldLength['createdBy']    = '30';
$config->workflowfield->default->fieldLength['createdDate']  = '';
$config->workflowfield->default->fieldLength['editedBy']     = '30';
$config->workflowfield->default->fieldLength['editedDate']   = '';
$config->workflowfield->default->fieldLength['assignedBy']   = '30';
$config->workflowfield->default->fieldLength['assignedDate'] = '';
$config->workflowfield->default->fieldLength['mailto']       = '';
$config->workflowfield->default->fieldLength['deleted']      = '10';

$config->workflowfield->default->controls['id']           = 'label';
$config->workflowfield->default->controls['parent']       = 'label';
$config->workflowfield->default->controls['assignedTo']   = 'select';
$config->workflowfield->default->controls['status']       = 'select';
$config->workflowfield->default->controls['createdBy']    = 'select';
$config->workflowfield->default->controls['createdDate']  = 'datetime';
$config->workflowfield->default->controls['editedBy']     = 'select';
$config->workflowfield->default->controls['editedDate']   = 'datetime';
$config->workflowfield->default->controls['assignedBy']   = 'select';
$config->workflowfield->default->controls['assignedDate'] = 'datetime';
$config->workflowfield->default->controls['mailto']       = 'multi-select';
$config->workflowfield->default->controls['deleted']      = 'radio';

$config->workflowfield->default->options['id']           = '[]';
$config->workflowfield->default->options['parent']       = '[]';
$config->workflowfield->default->options['assignedTo']   = 'user';
$config->workflowfield->default->options['status']       = '[]';
$config->workflowfield->default->options['createdBy']    = 'user';
$config->workflowfield->default->options['createdDate']  = '[]';
$config->workflowfield->default->options['editedBy']     = 'user';
$config->workflowfield->default->options['editedDate']   = '[]';
$config->workflowfield->default->options['assignedBy']   = 'user';
$config->workflowfield->default->options['assignedDate'] = '[]';
$config->workflowfield->default->options['mailto']       = 'user';
$config->workflowfield->default->options['deleted']      = $this->lang->workflowfield->default->options->deleted;

$config->workflowfield->default->values['id']           = '';
$config->workflowfield->default->values['parent']       = '0';
$config->workflowfield->default->values['assignedTo']   = '';
$config->workflowfield->default->values['status']       = '';
$config->workflowfield->default->values['createdBy']    = '';
$config->workflowfield->default->values['createdDate']  = '';
$config->workflowfield->default->values['editedBy']     = '';
$config->workflowfield->default->values['editedDate']   = '';
$config->workflowfield->default->values['assignedBy']   = '';
$config->workflowfield->default->values['assignedDate'] = '';
$config->workflowfield->default->values['mailto']       = '';
$config->workflowfield->default->values['deleted']      = '0';

$config->workflowfield->default->indexes = 'PRIMARY KEY `id` (`id`)';

$config->workflowfield->default->type          = 'varchar';
$config->workflowfield->default->optionClass   = 'varchar';
$config->workflowfield->default->charLength    = 50;
$config->workflowfield->default->varcharLength = 255;
$config->workflowfield->default->integerDigits = 10;
$config->workflowfield->default->decimalDigits = 2;
$config->workflowfield->default->maxInt        = 2147483647;
$config->workflowfield->default->minInt        = -2147483648;

$config->workflowfield->max = new stdclass();
$config->workflowfield->max->charLength    = 255;
$config->workflowfield->max->varcharLength = 1000;
$config->workflowfield->max->integerDigits = 12;
$config->workflowfield->max->decimalDigits = 5;
$config->workflowfield->max->tinyint       = pow(2, 7) - 1;
$config->workflowfield->max->smallint      = pow(2, 15) - 1;
$config->workflowfield->max->mediumint     = pow(2, 23) - 1;
$config->workflowfield->max->int           = pow(2, 31) - 1;

$config->workflowfield->min = new stdclass();
$config->workflowfield->min->charLength    = 1;
$config->workflowfield->min->varcharLength = 1;
$config->workflowfield->min->integerDigits = 1;
$config->workflowfield->min->decimalDigits = 1;
$config->workflowfield->min->tinyint       = -pow(2, 7);
$config->workflowfield->min->smallint      = -pow(2, 15);
$config->workflowfield->min->mediumint     = -pow(2, 23);
$config->workflowfield->min->int           = -pow(2, 31);

$config->workflowfield->formula = new stdclass();
$config->workflowfield->formula->operators['+'] = '＋';
$config->workflowfield->formula->operators['-'] = '－';
$config->workflowfield->formula->operators['*'] = '×';
$config->workflowfield->formula->operators['/'] = '÷';
$config->workflowfield->formula->operators['('] = '(';
$config->workflowfield->formula->operators[')'] = ')';

$config->workflowfield->formula->numbers = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, '.');

$config->workflowfield->excel = new stdclass();
$config->workflowfield->excel->templateFields = array('name', 'field', 'control', 'datasource', 'options', 'sql', 'defaultValue');
$config->workflowfield->excel->customWidth    = array('name' => 15, 'field' => 15, 'control' => 15, 'datasource' => 15, 'options' => 20, 'sql' => 20, 'defaultValue' => 15);
$config->workflowfield->excel->listFields     = array('control', 'datasource');
