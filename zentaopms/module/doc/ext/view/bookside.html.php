<?php $currentMethod = $this->app->getMethodName();?>
<?php $activeClass = (strpos(",browse,managebook,catalog,edit", ",{$this->methodName},") !== 'false' && isset($currentLib->id) && $currentLib->id == $libID) ? 'active' : '';?>
  <?php $serials  = $this->doc->computeSN($libID); ?>
  <?php $nodeList = $this->doc->getBookStructure($libID);?>
  <ul data-name="docsTree" data-ride="tree" data-initial-state="preserve" class="tree">
    <?php foreach($nodeList as $nodeInfo):?>
    <?php $serial = $nodeInfo->type != 'book' ? zget($nodeInfo->id, $serials, '') : '';?>
    <?php if($nodeInfo->parent != 0) continue;?>
    <?php $activeClass = (isset($doc->id) && $doc->id == $nodeInfo->id) ? 'active' : '';?>
    <?php $chapterNode = $nodeInfo->type == 'article' ? 'chapterNode' : '';?>
      <li <?php echo "class='open $activeClass $chapterNode'";?>>
        <?php if($nodeInfo->type == 'chapter'):?>
        <?php echo "<a title='{$nodeInfo->title}'>{$nodeInfo->title}</a>";?>
        <?php elseif($nodeInfo->type == 'article'):?>
        <?php if($currentMethod == 'tablecontents'):?>
        <span class="tail-info"><?php echo zget($users, $nodeInfo->editedBy) . ' &nbsp;' . $nodeInfo->editedDate;?></span>
        <?php endif;?>
        <?php echo "<span class='item doc-title'>{$serial} " . html::a(helper::createLink('doc', 'objectLibs', "type=book&objectID=0&libID=$libID&docID={$nodeInfo->id}"), "<i class='icon icon-file-text text-muted'></i> &nbsp;" . $nodeInfo->title, '', "title='{$nodeInfo->title}'") . '</span>';?>
        <?php endif;?>
        <?php if(!empty($nodeInfo->children)) $this->doc->getFrontCatalog($nodeInfo->children, $serials, isset($doc->id) ? $doc->id : 0);?>
      </li>
    <?php endforeach;?>
  </ul>
