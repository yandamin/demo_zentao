<?php if(!empty($properties['user'])):?>
<?php
$userMaxCount = $properties['user']['value'];
$userCount    = $this->dao->select("COUNT('*') as count")->from(TABLE_USER)
    ->where('deleted')->eq(0)
    ->beginIF(isset($this->config->bizVersion))->andWhere("feedback")->eq(0)->fi()
    ->fetch('count');
js::set('userCount', $userCount);
js::set('userMaxCount', $userMaxCount);
js::set('noticeUserCreate', str_replace('%maxcount%', $userMaxCount, $lang->user->noticeUserCreate));

if(isset($this->config->bizVersion))
{
    $feedbackCount    = $this->dao->select("COUNT('*') as count")->from(TABLE_USER)->where('deleted')->eq(0)->andWhere("feedback")->eq(1)->fetch('count');
    $feedbackMaxCount = $properties['feedback']['value'];
    js::set('feedbackCount', $feedbackCount);
    js::set('feedbackMaxCount', $feedbackMaxCount);
    js::set('noticeFeedbackCreate', str_replace('%maxcount%', $feedbackMaxCount, $lang->user->noticeFeedbackCreate));
}
?>
<script>
$(function()
{
    $('#submit').click(function()
    {
        var allUserCount = parseInt(userCount);
        <?php if(isset($this->config->bizVersion)):?>
        var allFeedbackCount = parseInt(feedbackCount);
        <?php endif;?>
        $('[id^="account"]').each(function()
        {
            if($(this).val())
            {
                <?php if(isset($this->config->bizVersion)):?>
                if($(this).closest('td').find(':checkbox[id^="feedback"]').prop('checked'))
                {
                    allFeedbackCount += 1;
                }
                else
                {
                    allUserCount += 1;
                }
                <?php else:?>
                allUserCount += 1;
                <?php endif;?>
            }
        });

        if(allUserCount > userMaxCount)
        {
            alert(noticeUserCreate.replace('%usercount%', allUserCount));
            return false;
        }
        <?php if(isset($this->config->bizVersion)):?>
        if(allFeedbackCount > feedbackMaxCount)
        {
            alert(noticeFeedbackCreate.replace('%usercount%', allFeedbackCount));
            return false;
        }
        <?php endif;?>
    })
})
</script>
<?php endif;?>
