<?php
$lang->user->expireWarning = "<p style='color:yellow'> 您的專業版授權將在%s天后到期，為避免影響使用，請及時續費。</p>";

$lang->user->noticeUserLimit = "專業版人數已經達到授權的上限，不能繼續添加用戶！";

$lang->user->noticeUserCreate     = "該版本限定用戶數為%maxcount%，添加後人數為%usercount%，將超出限制，請清除多餘用戶。";
$lang->user->noticeFeedbackCreate = "非研發用戶限定人數為%maxcount%，添加後人數為%usercount%，將超出限制，請清除多餘用戶。";
