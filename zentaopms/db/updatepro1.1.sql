ALTER TABLE  `zt_effort` ADD  `consumed` FLOAT NOT NULL AFTER  `date`;
ALTER TABLE `zt_user` ADD `ldap` CHAR(30) NOT NULL AFTER `ranzhi`;

REPLACE INTO `zt_grouppriv` (`group`, `module`, `method`) VALUES
(1,'ldap','index'),
(1,'ldap','set'),
(1,'user','importldap');
